package fintech.practice.csie.ntnu.edu.tw.fintechapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;

/**
 * Created by liebe on 16/6/15.
 */
public class Info extends AppCompatActivity{

    @BindView(R.id.odds)
    TextView odd;

    @BindView(R.id.hName)
    TextView hName;
    @BindView(R.id.hSpeed)
    TextView hSpeed;
    @BindView(R.id.hCond)
    TextView hCond;
    @BindView(R.id.iv_horse)
    ImageView horseImg;

    @BindView(R.id.rName)
    TextView rName;
    @BindView(R.id.rWeight)
    TextView rWeight;
    @BindView(R.id.rTech)
    TextView rTech;
    @BindView(R.id.rDescription)
    TextView rDescription;
    @BindView(R.id.iv_rider)
    ImageView riderImg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.info);
        buildViews();
    }

    public void buildViews(){
        Bundle bundle = this.getIntent().getExtras();

        hName = (TextView) findViewById(R.id.hName);
        hName.setText(bundle.getString("hName"));

        hSpeed = (TextView) findViewById(R.id.hSpeed);
        hSpeed.setText(bundle.getString("hSpeed"));

        hCond = (TextView) findViewById(R.id.hCond);
        hCond.setText(bundle.getString("hCond"));

        horseImg = (ImageView)findViewById(R.id.iv_horse);
        int i = bundle.getInt("HorseID");
        horseImg.setImageResource(Game.HorsePhoto[i-1]);

        rName = (TextView) findViewById(R.id.rName);
        rName.setText(bundle.getString("rName"));

        rWeight = (TextView) findViewById(R.id.rWeight);
        rWeight.setText(bundle.getString("rWeight"));

        rDescription = (TextView) findViewById(R.id.rDescription);
        rDescription.setText(bundle.getString("rDescript"));

        rTech = (TextView) findViewById(R.id.rTech);
        rTech.setText(bundle.getString("rTech"));

        riderImg = (ImageView)findViewById(R.id.iv_rider);
        int i1 = bundle.getInt("RiderID");
        riderImg.setImageResource(Game.RiderPhoto[i1-1]);


    }
}
