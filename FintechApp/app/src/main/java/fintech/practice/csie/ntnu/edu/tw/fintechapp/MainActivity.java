package fintech.practice.csie.ntnu.edu.tw.fintechapp;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.coinbase.android.sdk.OAuth;
import com.coinbase.api2.Coinbase;
import com.coinbase.api2.entity.OAuthTokensResponse;
import com.coinbase.api2.exception.UnauthorizedException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    /*@BindView(R.id.db)
    TextView db;*/
    @BindView(R.id.tv_account)
    TextView tvAccount;
    @BindView(R.id.tv_address)
    TextView tvAddress;
    @BindView(R.id.tv_balance)
    TextView tvBalance;
    @BindView(R.id.button)
    Button btnShowAddress;
    Handler h;

    private boolean login = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        h = new Handler();
        h.postDelayed(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(MainActivity.this, "Hello World", Toast.LENGTH_SHORT).show();
            }
        }, 1000L);
        //coinbase sdk usage document link: https://github.com/coinbase/coinbase-java
        //coinbase with testnet account sign-up: https://sandbox.coinbase.com/signup
        //coinbase sandbox document: https://developers.coinbase.com/docs/wallet/testing
    }

    public class CompleteAuthorizationTask extends AsyncTask<Void, Void, OAuthTokensResponse> {
        private Intent mIntent;

        public CompleteAuthorizationTask(Intent intent) {
            Log.d("fintech", "initial complete auth task");
            mIntent = intent;
        }

        @Override
        protected OAuthTokensResponse doInBackground(Void... params) {
            Log.d("fintech", "call complete authorization");
            try {
                return OAuth.completeAuthorization(MainActivity.this, Config.clientID, Config.clientSecret, mIntent.getData());
            } catch (UnauthorizedException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(OAuthTokensResponse r) {
            if (r == null) {
                Toast.makeText(MainActivity.this, "Login fail", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(MainActivity.this, "Login success", Toast.LENGTH_SHORT).show();
                ((FintechApplication) getApplication()).setOAuthResponse(r);
                Log.d("fintech", "token=" + r.getAccessToken());
            }
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        Log.d("fintech", "got new intent");

        if (intent != null && intent.getAction() != null && intent.getAction().equals("android.intent.action.VIEW")) {
            Log.d("fintech", "ready to execute CompleteAuthorizationTask");
            new CompleteAuthorizationTask(intent).execute();
        }
    }

    @OnClick(R.id.button)
    public void login() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    OAuth.beginAuthorization(MainActivity.this, Config.clientID, "user", "fintech://coinbase-oauth", null);
                } catch (Exception e) {
                    Log.e("fintech", null, e);
                }
                return null;
            }
        }.execute();
        login = true;
    }

    @OnClick(R.id.button2)
    public void showInfo(View v) {
        if(login == false) return;

        Intent i = new Intent();
        i.setClass(MainActivity.this, Game.class);

        new AsyncTask<Void, Void, Wallet>() {
            @Override
            protected Wallet doInBackground(Void... params) {
                Coinbase cb = ((FintechApplication) getApplication()).getCoinbase();
                Wallet w = new Wallet();
                try {
                    URL url = new URL("http://140.122.184.227/~40147029S/login.php");
                    HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
                    httpURLConnection.setRequestMethod("POST");
                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.setDoInput(true);
                    OutputStream outputStream = httpURLConnection.getOutputStream();
                    BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream,"UTF-8"));
                    String data = URLEncoder.encode("E_mail","UTF-8")+"="+ URLEncoder.encode(cb.getUser().getEmail().toString(),"UTF-8")+"&"+
                            URLEncoder.encode("Name","UTF-8")+"="+URLEncoder.encode(cb.getUser().getName().toString(),"UTF-8");
                    bufferedWriter.write(data);
                    bufferedWriter.flush();
                    bufferedWriter.close();
                    outputStream.close();
                    InputStream inputStream = httpURLConnection.getInputStream();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream,"iso-8859-1"));
                    String response = "";
                    String line = "";
                    while ((line = bufferedReader.readLine())!=null)
                    {
                        response+= line;
                    }
                    bufferedReader.close();
                    inputStream.close();
                    httpURLConnection.disconnect();

                    w.setAccount(cb.getUser().getEmail().toString());
                    w.setAddress(cb.getUser().getName().toString());
                    w.setBalance(response);

                    Log.d("fintech", "address=" + w.getAddress());
                } catch (Exception e) {
                    Log.e("fintech", null, e);
                    //Toast.makeText(this,e.getMessage(),Toast.LENGTH_SHORT).show();
                }
                return w;
            }

            @Override
            protected void onPostExecute(Wallet w) {
                tvAccount.setText(w.getAccount());
                tvAddress.setText(w.getAddress());
                tvBalance.setText(w.getBalance());
            }
        }.execute();

        startActivity(i);
    }

}
