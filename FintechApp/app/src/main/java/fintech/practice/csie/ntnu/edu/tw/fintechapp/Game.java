package fintech.practice.csie.ntnu.edu.tw.fintechapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by liebe on 16/6/14.
 */

public class Game extends AppCompatActivity{

    //@BindView(R.id.db)
    //TextView db;

    //Bind 5 horse name
    @BindView(R.id.tv_horse1)
    TextView hName1;
    @BindView(R.id.tv_horse2)
    TextView hName2;
    @BindView(R.id.tv_horse3)
    TextView hName3;
    @BindView(R.id.tv_horse4)
    TextView hName4;
    @BindView(R.id.tv_horse5)
    TextView hName5;

    //Bind 5 Rider name
    @BindView(R.id.tv_rider1)
    TextView rName1;
    @BindView(R.id.tv_rider2)
    TextView rName2;
    @BindView(R.id.tv_rider3)
    TextView rName3;
    @BindView(R.id.tv_rider4)
    TextView rName4;
    @BindView(R.id.tv_rider5)
    TextView rName5;


    //Bind 5 horse
    @BindView(R.id.iv_horse1)
    ImageView horseImg1;
    @BindView(R.id.iv_horse2)
    ImageView horseImg2;
    @BindView(R.id.iv_horse3)
    ImageView horseImg3;
    @BindView(R.id.iv_horse4)
    ImageView horseImg4;
    @BindView(R.id.iv_horse5)
    ImageView horseImg5;

    //Bind 5 riders
    @BindView(R.id.iv_rider1)
    ImageView riderImg1;
    @BindView(R.id.iv_rider2)
    ImageView riderImg2;
    @BindView(R.id.iv_rider3)
    ImageView riderImg3;
    @BindView(R.id.iv_rider4)
    ImageView riderImg4;
    @BindView(R.id.iv_rider5)
    ImageView riderImg5;

    public float money = 1000;
    private Button btn_go;

    String showUrl = "http://140.122.184.227/~40147029S/connect.php";
    RequestQueue requestQueue;

    // Horse Information
    public static int[] HorseID = new int[15];
    public static String[] HorseName = new String[15];
    public static String[] HorseSpeed = new String[15];


    // Rider Information
    public static int[] RiderID = new int[15];
    public static String[] RiderName = new String[15];
    public static String[] RiderWeight = new String[15];
    public static String[] RiderLevel = new String[15];
    public static String[] RiderDescript = new String[15];

    public static Integer[] HorsePhoto = {
            R.drawable.horse_01, R.drawable.horse_02, R.drawable.horse_03,
            R.drawable.horse_04, R.drawable.horse_05, R.drawable.horse_06,
            R.drawable.horse_07, R.drawable.horse_08, R.drawable.horse_09
    };

    public static Integer[] RiderPhoto = {
            R.drawable.rider_01, R.drawable.rider_02, R.drawable.rider_03,
            R.drawable.rider_04, R.drawable.rider_05, R.drawable.rider_06,
            R.drawable.rider_07, R.drawable.rider_08
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.game);

        ButterKnife.bind(this);
        requestQueue = Volley.newRequestQueue(getApplicationContext());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.POST,showUrl, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        System.out.println(response.toString());
                        try {
                            JSONArray data = response.getJSONArray("data");
                            //這邊要和上面json的名稱一樣
                            //下邊是把全部資料都印出來
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject jasondata = data.getJSONObject(i);
                                if(i<5){
                                    HorseName[i] = jasondata.getString("name");
                                    HorseID[i] = jasondata.getInt("id");
                                    HorseSpeed[i] = jasondata.getString("speed");
                                    //db.append(HorseName[i] + " " + HorseID[i] + " " + HorseSpeed[i]+" \n");
                                }
                                else{
                                    RiderName[i-5] = jasondata.getString("name");
                                    RiderID[i-5] = jasondata.getInt("id");
                                    RiderWeight[i-5] = jasondata.getString("weight");
                                    RiderLevel[i-5] = jasondata.getString("level");
                                    RiderDescript[i-5] = jasondata.getString("description");
                                    //db.append(RiderName[i-5] + " " + RiderID[i-5] + " " + RiderWeight[i-5] + "\n");
                                }

                                //db是textview
                            }
                            //db.append(Game.HorseName[4] + " " + Game.HorseID[4] + " " + Game.HorseSpeed[4]+" \n");
                            //db.append("===\n");//把資料放到textview顯示出來
                            //db.setText(HorseName[0]);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        System.out.append(error.getMessage());
                    }
                });
        requestQueue.add(jsonObjectRequest);

        buildViews();


        btn_go = (Button) findViewById(R.id.button);
        btn_go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Game.this, Info.class);

                Bundle bundle = new Bundle();
                bundle.putString("hName", HorseName[0]);
                bundle.putString("hSpeed", HorseSpeed[0]);
                bundle.putString("hCond", "好");
                bundle.putInt("HorseID", HorseID[0]);

                bundle.putString("rName", RiderName[0]);
                bundle.putString("rWeight", RiderWeight[0]);
                bundle.putString("rTech", RiderLevel[0]);
                bundle.putString("rDescript", RiderDescript[0]);
                bundle.putInt("RiderID", RiderID[0]);

                intent.putExtras(bundle);
                startActivity(intent);
            }
        });


        btn_go = (Button) findViewById(R.id.button2);
        btn_go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Game.this, Info.class);

                Bundle bundle = new Bundle();
                bundle.putString("hName", HorseName[1]);
                bundle.putString("hSpeed", HorseSpeed[1]);
                bundle.putString("hCond", "好");
                bundle.putInt("HorseID", HorseID[1]);

                bundle.putString("rName", RiderName[1]);
                bundle.putString("rWeight", RiderWeight[1]);
                bundle.putString("rTech", RiderLevel[1]);
                bundle.putString("rDescript", RiderDescript[1]);
                bundle.putInt("RiderID", RiderID[1]);

                intent.putExtras(bundle);
                startActivity(intent);
            }
        });


        btn_go = (Button) findViewById(R.id.button3);
        btn_go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Game.this, Info.class);

                Bundle bundle = new Bundle();
                bundle.putString("hName", HorseName[2]);
                bundle.putString("hSpeed", HorseSpeed[2]);
                bundle.putString("hCond", "好");
                bundle.putInt("HorseID", HorseID[2]);

                bundle.putString("rName", RiderName[2]);
                bundle.putString("rWeight", RiderWeight[2]);
                bundle.putString("rTech", RiderLevel[2]);
                bundle.putString("rDescript", RiderDescript[2]);
                bundle.putInt("RiderID", RiderID[2]);

                intent.putExtras(bundle);

                startActivity(intent);
            }
        });


        btn_go = (Button) findViewById(R.id.button4);
        btn_go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Game.this, Info.class);

                Bundle bundle = new Bundle();
                bundle.putString("hName", HorseName[3]);
                bundle.putString("hSpeed", HorseSpeed[3]);
                bundle.putString("hCond", "好");
                bundle.putInt("HorseID", HorseID[3]);

                bundle.putString("rName", RiderName[3]);
                bundle.putString("rWeight", RiderWeight[3]);
                bundle.putString("rTech", RiderLevel[3]);
                bundle.putString("rDescript", RiderDescript[3]);
                bundle.putInt("RiderID", RiderID[3]);

                intent.putExtras(bundle);

                startActivity(intent);
            }
        });


        btn_go = (Button) findViewById(R.id.button5);
        btn_go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Game.this, Info.class);

                Bundle bundle = new Bundle();
                bundle.putString("hName", HorseName[4]);
                bundle.putString("hSpeed", HorseSpeed[4]);
                bundle.putString("hCond", "好");
                bundle.putInt("HorseID", HorseID[4]);

                bundle.putString("rName", RiderName[4]);
                bundle.putString("rWeight", RiderWeight[4]);
                bundle.putString("rTech", RiderLevel[4]);
                bundle.putString("rDescript", RiderDescript[4]);
                bundle.putInt("RiderID", RiderID[4]);

                intent.putExtras(bundle);
                startActivity(intent);
            }
        });


    }

    public void buildViews(){

        hName1 = (TextView)findViewById(R.id.tv_horse1);
        hName2 = (TextView)findViewById(R.id.tv_horse2);
        hName3 = (TextView)findViewById(R.id.tv_horse3);
        hName4 = (TextView)findViewById(R.id.tv_horse4);
        hName5 = (TextView)findViewById(R.id.tv_horse5);

        hName1.setText(HorseName[0]);
        hName2.setText(HorseName[1]);
        hName3.setText(HorseName[2]);
        hName4.setText(HorseName[3]);
        hName5.setText(HorseName[4]);

        horseImg1 = (ImageView)findViewById(R.id.iv_horse1);
        horseImg2 = (ImageView)findViewById(R.id.iv_horse2);
        horseImg3 = (ImageView)findViewById(R.id.iv_horse3);
        horseImg4 = (ImageView)findViewById(R.id.iv_horse4);
        horseImg5 = (ImageView)findViewById(R.id.iv_horse5);

        for(int i=0; i<=9; i+=1){
            if(HorseID[0]==i+1) horseImg1.setImageResource(HorsePhoto[i]);
            if(HorseID[1]==i+1) horseImg2.setImageResource(HorsePhoto[i]);
            if(HorseID[2]==i+1) horseImg3.setImageResource(HorsePhoto[i]);
            if(HorseID[3]==i+1) horseImg4.setImageResource(HorsePhoto[i]);
            if(HorseID[4]==i+1) horseImg5.setImageResource(HorsePhoto[i]);
        }


        rName1 = (TextView) findViewById(R.id.tv_rider1);
        rName2 = (TextView) findViewById(R.id.tv_rider2);
        rName3 = (TextView) findViewById(R.id.tv_rider3);
        rName4 = (TextView) findViewById(R.id.tv_rider4);
        rName5 = (TextView) findViewById(R.id.tv_rider5);

        rName1.setText(RiderName[0]);
        rName2.setText(RiderName[1]);
        rName3.setText(RiderName[2]);
        rName4.setText(RiderName[3]);
        rName5.setText(RiderName[4]);

        riderImg1 = (ImageView)findViewById(R.id.iv_rider1);
        riderImg2 = (ImageView)findViewById(R.id.iv_rider2);
        riderImg3 = (ImageView)findViewById(R.id.iv_rider3);
        riderImg4 = (ImageView)findViewById(R.id.iv_rider4);
        riderImg5 = (ImageView)findViewById(R.id.iv_rider5);


        for(int i=0; i<8; i+=1){
            if(RiderID[0]==i+1) riderImg1.setImageResource(RiderPhoto[i]);
            if(RiderID[1]==i+1) riderImg2.setImageResource(RiderPhoto[i]);
            if(RiderID[2]==i+1) riderImg3.setImageResource(RiderPhoto[i]);
            if(RiderID[3]==i+1) riderImg4.setImageResource(RiderPhoto[i]);
            if(RiderID[4]==i+1) riderImg5.setImageResource(RiderPhoto[i]);
        }
    }


}
