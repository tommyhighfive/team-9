package com.coinbase.api2.entity;

import java.io.Serializable;

public class UserNode implements Serializable {

        private static final long serialVersionUID = -6543933340956648721L;
        private com.coinbase.api2.entity.User _user;

	public com.coinbase.api2.entity.User getUser() {
		return _user;
	}

	public void setUser(com.coinbase.api2.entity.User user) {
		_user = user;
	}
	
}
