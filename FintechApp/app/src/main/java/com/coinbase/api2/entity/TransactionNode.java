package com.coinbase.api2.entity;

import java.io.Serializable;

public class TransactionNode implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 7122426900695444540L;
    private com.coinbase.api2.entity.Transaction _transaction;

    public com.coinbase.api2.entity.Transaction getTransaction() {
	return _transaction;
    }

    public void setTransaction(com.coinbase.api2.entity.Transaction transaction) {
	_transaction = transaction;
    }

}
