package com.coinbase.api2.deserializer;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.util.StdConverter;

public class ReportsLifter extends StdConverter<List<com.coinbase.api2.entity.ReportNode>, List<com.coinbase.api2.entity.Report>> {

    public List<com.coinbase.api2.entity.Report> convert(List<com.coinbase.api2.entity.ReportNode> nodes) {
	ArrayList<com.coinbase.api2.entity.Report> result = new ArrayList<com.coinbase.api2.entity.Report>();
	
	for (com.coinbase.api2.entity.ReportNode node : nodes) {
	    result.add(node.getReport());
	}
	
	return result;
    }

}
