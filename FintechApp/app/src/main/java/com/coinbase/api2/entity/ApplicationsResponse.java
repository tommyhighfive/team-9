package com.coinbase.api2.entity;

import com.coinbase.api2.entity.Application;

import java.util.List;

public class ApplicationsResponse extends Response {
    /**
     * 
     */
    private static final long serialVersionUID = -9050344897579847790L;
    private List<com.coinbase.api2.entity.Application> _applications;

    public List<com.coinbase.api2.entity.Application> getApplications() {
        return _applications;
    }

    public void setApplications(List<Application> applications) {
        _applications = applications;
    }
}
