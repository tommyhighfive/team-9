package com.coinbase.api2.entity;

import java.io.Serializable;

public class PaymentMethodNode implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = -3467480965918383878L;
    private com.coinbase.api2.entity.PaymentMethod _paymentMethod;

    public com.coinbase.api2.entity.PaymentMethod getPaymentMethod() {
        return _paymentMethod;
    }

    public void setPaymentMethod(com.coinbase.api2.entity.PaymentMethod paymentMethod) {
        _paymentMethod = paymentMethod;
    }
}
