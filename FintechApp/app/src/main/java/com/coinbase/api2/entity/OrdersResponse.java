package com.coinbase.api2.entity;

import java.util.List;

import com.coinbase.api2.deserializer.OrdersLifter;
import com.coinbase.api2.entity.Order;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

public class OrdersResponse extends com.coinbase.api2.entity.Response {
    /**
     * 
     */
    private static final long serialVersionUID = 2721898279676340667L;
    private List<com.coinbase.api2.entity.Order> _orders;
    
    public List<com.coinbase.api2.entity.Order> getOrders() {
	return _orders;
    }

    @JsonDeserialize(converter=OrdersLifter.class)
    public void setOrders(List<Order> orders) {
	_orders = orders;
    }
}
