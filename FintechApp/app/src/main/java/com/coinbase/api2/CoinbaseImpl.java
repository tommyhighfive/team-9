package com.coinbase.api2;

import com.coinbase.api2.entity.Account;
import com.coinbase.api2.entity.AccountChangesResponse;
import com.coinbase.api2.entity.AddressResponse;
import com.coinbase.api2.entity.Application;
import com.coinbase.api2.entity.ApplicationResponse;
import com.coinbase.api2.entity.ApplicationsResponse;
import com.coinbase.api2.entity.ButtonResponse;
import com.coinbase.api2.entity.ContactsResponse;
import com.coinbase.api2.entity.OAuthCodeResponse;
import com.coinbase.api2.entity.OAuthTokensRequest;
import com.coinbase.api2.entity.OAuthTokensResponse;
import com.coinbase.api2.entity.RecurringPayment;
import com.coinbase.api2.entity.RecurringPaymentResponse;
import com.coinbase.api2.entity.ReportResponse;
import com.coinbase.api2.entity.ReportsResponse;
import com.coinbase.api2.entity.Request;
import com.coinbase.api2.entity.Response;
import com.coinbase.api2.entity.TokenResponse;
import com.coinbase.api2.entity.Transaction;
import com.coinbase.api2.entity.TransactionResponse;
import com.coinbase.api2.entity.User;
import com.coinbase.api2.exception.CoinbaseException;
import com.coinbase.api2.exception.CredentialsIncorrectException;
import com.coinbase.api2.exception.TwoFactorIncorrectException;
import com.coinbase.api2.exception.UnauthorizedException;
import com.coinbase.api2.exception.UnspecifiedAccount;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.io.IOUtils;
import org.joda.money.CurrencyUnit;
import org.joda.money.IllegalCurrencyException;
import org.joda.money.Money;
import org.joda.time.DateTime;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;

import au.com.bytecode.opencsv.CSVReader;

import org.apache.http.client.utils.URIBuilder;

class CoinbaseImpl implements com.coinbase.api2.Coinbase {

    private static final ObjectMapper objectMapper = ObjectMapperProvider.createDefaultMapper();

    private URL    _baseApiUrl;
    private URL    _baseOAuthUrl;
    private String _accountId;
    private String _apiKey;
    private String _apiSecret;
    private String _accessToken;
    private SSLContext _sslContext;
    private SSLSocketFactory _socketFactory;
    private CallbackVerifier _callbackVerifier;

    CoinbaseImpl(CoinbaseBuilder builder) {

        _baseApiUrl = builder.base_api_url;
        _baseOAuthUrl = builder.base_oauth_url;
        _apiKey = builder.api_key;
        _apiSecret = builder.api_secret;
        _accessToken = builder.access_token;
        _accountId = builder.acct_id;
        _sslContext = builder.ssl_context;
        _callbackVerifier = builder.callback_verifier;

        try {
            if (_baseApiUrl == null) {
                _baseApiUrl = new URL("https://coinbase.com/api/v1/");
            }
            if (_baseOAuthUrl == null) {
                _baseOAuthUrl = new URL("https://coinbase.com/oauth/");
            }
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }

        // Register BTC as a currency since Android won't let joda read from classpath resources
        try {
            CurrencyUnit.registerCurrency("BTC", -1, 8, new ArrayList<String>());
        } catch (IllegalArgumentException ex) {}

        if (_sslContext != null) {
            _socketFactory = _sslContext.getSocketFactory();
        } else {
            _socketFactory = CoinbaseSSL.getSSLContext().getSocketFactory();
        }

        if (_callbackVerifier == null) {
            _callbackVerifier = new CallbackVerifierImpl();
        }
    }

    @Override
    public com.coinbase.api2.entity.User getUser() throws IOException, com.coinbase.api2.exception.CoinbaseException {
        URL usersUrl;
        try {
            usersUrl = new URL(_baseApiUrl, "users");
        } catch (MalformedURLException ex) {
            throw new AssertionError(ex);
        }
        return get(usersUrl, com.coinbase.api2.entity.UsersResponse.class).getUsers().get(0).getUser();
    }

    @Override
    public com.coinbase.api2.entity.AccountsResponse getAccounts() throws IOException, com.coinbase.api2.exception.CoinbaseException {
        return getAccounts(1, 25, false);
    }

    @Override
    public com.coinbase.api2.entity.AccountsResponse getAccounts(int page) throws IOException, com.coinbase.api2.exception.CoinbaseException {
        return getAccounts(page, 25, false);
    }

    @Override
    public com.coinbase.api2.entity.AccountsResponse getAccounts(int page, int limit) throws IOException, com.coinbase.api2.exception.CoinbaseException {
        return getAccounts(page, limit, false);
    }

    @Override
    public com.coinbase.api2.entity.AccountsResponse getAccounts(int page, int limit, boolean includeInactive) throws IOException, com.coinbase.api2.exception.CoinbaseException {
        URL accountsUrl;
        try {
            accountsUrl = new URL(
                    _baseApiUrl,
                    "accounts?" +
                            "&page=" + page +
                            "&limit=" + limit +
                            "&all_accounts=" + (includeInactive ? "true" : "false")
            );
        } catch (MalformedURLException ex) {
            throw new AssertionError(ex);
        }

        return get(accountsUrl, com.coinbase.api2.entity.AccountsResponse.class);
    }

    @Override
    public Money getBalance() throws IOException, com.coinbase.api2.exception.CoinbaseException {
        if (_accountId != null) {
            return getBalance(_accountId);
        } else {
            throw new com.coinbase.api2.exception.UnspecifiedAccount();
        }
    }

    @Override
    public Money getBalance(String accountId) throws IOException, com.coinbase.api2.exception.CoinbaseException {
        URL accountBalanceUrl;
        try {
            accountBalanceUrl = new URL(_baseApiUrl, "accounts/" + accountId + "/balance");
        } catch (MalformedURLException ex) {
            throw new com.coinbase.api2.exception.CoinbaseException("Invalid account id");
        }
        return deserialize(doHttp(accountBalanceUrl, "GET", null), Money.class);
    }

    @Override
    public void setPrimaryAccount(String accountId) throws com.coinbase.api2.exception.CoinbaseException, IOException {
        URL setPrimaryUrl;
        try {
            setPrimaryUrl = new URL(_baseApiUrl, "accounts/" + accountId + "/primary");
        } catch (MalformedURLException ex) {
            throw new com.coinbase.api2.exception.CoinbaseException("Invalid account id");
        }
        post(setPrimaryUrl, new Request(), com.coinbase.api2.entity.Response.class);
    }

    @Override
    public void deleteAccount(String accountId) throws com.coinbase.api2.exception.CoinbaseException, IOException {
        URL accountUrl;
        try {
            accountUrl = new URL(_baseApiUrl, "accounts/" + accountId);
        } catch (MalformedURLException ex) {
            throw new com.coinbase.api2.exception.CoinbaseException("Invalid account id");
        }
        delete(accountUrl, com.coinbase.api2.entity.Response.class);
    }

    @Override
    public void setPrimaryAccount() throws com.coinbase.api2.exception.CoinbaseException, IOException {
        if (_accountId != null) {
            setPrimaryAccount(_accountId);
        } else {
            throw new com.coinbase.api2.exception.UnspecifiedAccount();
        }
    }

    @Override
    public void deleteAccount() throws com.coinbase.api2.exception.CoinbaseException, IOException {
        if (_accountId != null) {
            deleteAccount(_accountId);
        } else {
            throw new UnspecifiedAccount();
        }
    }

    @Override
    public void updateAccount(Account account) throws com.coinbase.api2.exception.CoinbaseException, IOException, com.coinbase.api2.exception.UnspecifiedAccount {
        if (_accountId != null) {
            updateAccount(_accountId, account);
        } else {
            throw new com.coinbase.api2.exception.UnspecifiedAccount();
        }
    }

    @Override
    public com.coinbase.api2.entity.Account createAccount(Account account) throws com.coinbase.api2.exception.CoinbaseException, IOException {
        URL accountsUrl;
        try {
            accountsUrl = new URL(_baseApiUrl, "accounts");
        } catch (MalformedURLException ex) {
            throw new AssertionError(ex);
        }

        Request request = new Request();
        request.setAccount(account);

        return post(accountsUrl, request, com.coinbase.api2.entity.AccountResponse.class).getAccount();
    }

    @Override
    public void updateAccount(String accountId, Account account) throws com.coinbase.api2.exception.CoinbaseException, IOException {
        URL accountUrl;
        try {
            accountUrl = new URL(_baseApiUrl, "accounts/" + accountId);
        } catch (MalformedURLException ex) {
            throw new com.coinbase.api2.exception.CoinbaseException("Invalid account id");
        }

        com.coinbase.api2.entity.Request request = new Request();
        request.setAccount(account);
        put(accountUrl, request, com.coinbase.api2.entity.Response.class);
    }

    @Override
    public Money getSpotPrice(CurrencyUnit currency) throws IOException, com.coinbase.api2.exception.CoinbaseException {
        URL spotPriceUrl;
        try {
            spotPriceUrl = new URL(_baseApiUrl, "prices/spot_rate?currency=" + URLEncoder.encode(currency.getCurrencyCode(), "UTF-8"));
        } catch (MalformedURLException ex) {
            throw new AssertionError(ex);
        }
        return deserialize(doHttp(spotPriceUrl, "GET", null), Money.class);
    }

    @Override
    public com.coinbase.api2.entity.Quote getBuyQuote(Money amount) throws IOException, com.coinbase.api2.exception.CoinbaseException {
        return getBuyQuote(amount, null);
    }

    @Override
    public com.coinbase.api2.entity.Quote getBuyQuote(Money amount, String paymentMethodId) throws IOException, com.coinbase.api2.exception.CoinbaseException {
        String qtyParam;
        if(amount.getCurrencyUnit().getCode().equals("BTC")) {
            qtyParam = "qty";
        }
        else {
            qtyParam = "native_qty";
        }

        URL buyPriceUrl;
        try {
            buyPriceUrl = new URL(
                    _baseApiUrl,
                    "prices/buy?" + qtyParam +"=" + URLEncoder.encode(amount.getAmount().toPlainString(), "UTF-8") +
                            (_accountId != null ? "&account_id=" + _accountId : "") +
                            (paymentMethodId != null ? "&payment_method_id=" + paymentMethodId : "")
            );
        } catch (MalformedURLException ex) {
            throw new AssertionError(ex);
        }
        return deserialize(doHttp(buyPriceUrl, "GET", null), com.coinbase.api2.entity.Quote.class);
    }

    @Override
    public com.coinbase.api2.entity.Quote getSellQuote(Money amount) throws IOException, com.coinbase.api2.exception.CoinbaseException {
        return getSellQuote(amount, null);
    }

    @Override
    public com.coinbase.api2.entity.Quote getSellQuote(Money amount, String paymentMethodId) throws IOException, com.coinbase.api2.exception.CoinbaseException {
        String qtyParam;
        if(amount.getCurrencyUnit().getCode().equals("BTC")) {
            qtyParam = "qty";
        }
        else {
            qtyParam = "native_qty";
        }

        URL sellPriceUrl;
        try {
            sellPriceUrl = new URL(
                    _baseApiUrl,
                    "prices/sell?" + qtyParam + "=" + URLEncoder.encode(amount.getAmount().toPlainString(), "UTF-8") +
                            (_accountId != null ? "&account_id=" + _accountId : "") +
                            (paymentMethodId != null ? "&payment_method_id=" + paymentMethodId : "")
            );
        } catch (MalformedURLException ex) {
            throw new AssertionError(ex);
        }
        return deserialize(doHttp(sellPriceUrl, "GET", null), com.coinbase.api2.entity.Quote.class);
    }

    @Override
    public com.coinbase.api2.entity.TransactionsResponse getTransactions(int page) throws IOException, com.coinbase.api2.exception.CoinbaseException {
        URL transactionsUrl;
        try {
            transactionsUrl = new URL(
                    _baseApiUrl,
                    "transactions?page=" + page +
                            (_accountId != null ? "&account_id=" + _accountId : "")
            );
        } catch (MalformedURLException ex) {
            throw new com.coinbase.api2.exception.CoinbaseException("Invalid account id");
        }
        return get(transactionsUrl, com.coinbase.api2.entity.TransactionsResponse.class);
    }

    @Override
    public com.coinbase.api2.entity.TransactionsResponse getTransactions() throws IOException, com.coinbase.api2.exception.CoinbaseException {
        return getTransactions(1);
    }

    @Override
    public com.coinbase.api2.entity.Transaction getTransaction(String id) throws IOException, com.coinbase.api2.exception.CoinbaseException {
        URL transactionUrl;
        try {
            transactionUrl = new URL(_baseApiUrl, "transactions/" + id);
        } catch (MalformedURLException ex) {
            throw new com.coinbase.api2.exception.CoinbaseException("Invalid transaction id");
        }
        return get(transactionUrl, TransactionResponse.class).getTransaction();
    }

    @Override
    public Transaction requestMoney(com.coinbase.api2.entity.Transaction transaction) throws com.coinbase.api2.exception.CoinbaseException, IOException {
        URL requestMoneyUrl;
        try {
            requestMoneyUrl = new URL(_baseApiUrl, "transactions/request_money");
        } catch (MalformedURLException ex) {
            throw new AssertionError(ex);
        }

        if (transaction.getAmount() == null) {
            throw new com.coinbase.api2.exception.CoinbaseException("Amount is a required field");
        }

        Request request = newAccountSpecificRequest();
        request.setTransaction(transaction);

        return post(requestMoneyUrl, request, TransactionResponse.class).getTransaction();
    }

    @Override
    public void resendRequest(String id) throws com.coinbase.api2.exception.CoinbaseException, IOException {
        URL resendRequestUrl;
        try {
            resendRequestUrl = new URL(_baseApiUrl, "transactions/" + id + "/resend_request");
        } catch (MalformedURLException ex) {
            throw new com.coinbase.api2.exception.CoinbaseException("Invalid transaction id");
        }
        put(resendRequestUrl, newAccountSpecificRequest(), com.coinbase.api2.entity.Response.class);
    }

    @Override
    public void deleteRequest(String id) throws com.coinbase.api2.exception.CoinbaseException, IOException {
        URL cancelRequestUrl;
        try {
            cancelRequestUrl = new URL(_baseApiUrl, "transactions/" + id + "/cancel_request");
        } catch (MalformedURLException ex) {
            throw new com.coinbase.api2.exception.CoinbaseException("Invalid transaction id");
        }
        delete(cancelRequestUrl, com.coinbase.api2.entity.Response.class);
    }

    @Override
    public Transaction completeRequest(String id) throws com.coinbase.api2.exception.CoinbaseException, IOException {
        URL completeRequestUrl;
        try {
            completeRequestUrl = new URL(_baseApiUrl, "transactions/" + id + "/complete_request");
        } catch (MalformedURLException ex) {
            throw new com.coinbase.api2.exception.CoinbaseException("Invalid transaction id");
        }
        return put(completeRequestUrl, newAccountSpecificRequest(),TransactionResponse.class).getTransaction();
    }

    @Override
    public Transaction sendMoney(Transaction transaction) throws CoinbaseException, IOException {
        URL sendMoneyUrl;
        try {
            sendMoneyUrl = new URL(_baseApiUrl, "transactions/send_money");
        } catch (MalformedURLException ex) {
            throw new AssertionError(ex);
        }

        if (transaction.getAmount() == null) {
            throw new com.coinbase.api2.exception.CoinbaseException("Amount is a required field");
        }

        com.coinbase.api2.entity.Request request = newAccountSpecificRequest();
        request.setTransaction(transaction);

        return post(sendMoneyUrl, request, TransactionResponse.class).getTransaction();
    }

    @Override
    public com.coinbase.api2.entity.TransfersResponse getTransfers(int page) throws IOException, com.coinbase.api2.exception.CoinbaseException {
        URL transfersUrl;
        try {
            transfersUrl = new URL(
                    _baseApiUrl,
                    "transfers?page=" + page +
                            (_accountId != null ? "&account_id=" + _accountId : "")
            );
        } catch (MalformedURLException ex) {
            throw new com.coinbase.api2.exception.CoinbaseException("Invalid account id");
        }
        return get(transfersUrl, com.coinbase.api2.entity.TransfersResponse.class);
    }

    @Override
    public com.coinbase.api2.entity.TransfersResponse getTransfers() throws IOException, com.coinbase.api2.exception.CoinbaseException {
        return getTransfers(1);
    }

    @Override
    public com.coinbase.api2.entity.Transfer sell(Money amount) throws com.coinbase.api2.exception.CoinbaseException, IOException {
        return sell(amount, null);
    }

    @Override
    public com.coinbase.api2.entity.Transfer sell(Money amount, String paymentMethodId) throws com.coinbase.api2.exception.CoinbaseException, IOException {
        return sell(amount, paymentMethodId, null);
    }

    @Override
    public com.coinbase.api2.entity.Transfer sell(Money amount, String paymentMethodId, Boolean commit) throws com.coinbase.api2.exception.CoinbaseException, IOException {
        URL sellsUrl;
        try {
            sellsUrl = new URL(_baseApiUrl, "sells");
        } catch (MalformedURLException ex) {
            throw new AssertionError(ex);
        }

        com.coinbase.api2.entity.Request request = newAccountSpecificRequest();
        request.setQty(amount.getAmount().doubleValue());
        request.setPaymentMethodId(paymentMethodId);
        request.setCurrency(amount.getCurrencyUnit().getCurrencyCode());
        request.setCommit(commit);

        return post(sellsUrl, request, com.coinbase.api2.entity.TransferResponse.class).getTransfer();
    }

    @Override
    public com.coinbase.api2.entity.Transfer buy(Money amount) throws com.coinbase.api2.exception.CoinbaseException, IOException {
        return buy(amount, null);
    }

    @Override
    public com.coinbase.api2.entity.Transfer buy(Money amount, String paymentMethodId) throws com.coinbase.api2.exception.CoinbaseException, IOException {
        return buy(amount, paymentMethodId, null);
    }

    @Override
    public com.coinbase.api2.entity.Transfer buy(Money amount, String paymentMethodId, Boolean commit) throws com.coinbase.api2.exception.CoinbaseException, IOException {
        URL buysUrl;
        try {
            buysUrl = new URL(_baseApiUrl, "buys");
        } catch (MalformedURLException ex) {
            throw new AssertionError(ex);
        }

        com.coinbase.api2.entity.Request request = newAccountSpecificRequest();
        request.setQty(amount.getAmount().doubleValue());
        request.setPaymentMethodId(paymentMethodId);
        request.setCurrency(amount.getCurrencyUnit().getCurrencyCode());
        request.setCommit(commit);

        return post(buysUrl, request, com.coinbase.api2.entity.TransferResponse.class).getTransfer();
    }

    @Override
    public com.coinbase.api2.entity.Transfer commitTransfer(String transactionId) throws com.coinbase.api2.exception.CoinbaseException, IOException {
        URL commitUrl;
        try {
            commitUrl = new URL(_baseApiUrl, "transfers/" + transactionId + "/commit");
        } catch (MalformedURLException ex) {
            throw new com.coinbase.api2.exception.CoinbaseException("Invalid transaction id");
        }

        return post(commitUrl, null, com.coinbase.api2.entity.TransferResponse.class).getTransfer();
    }

    @Override
    public com.coinbase.api2.entity.Order getOrder(String idOrCustom) throws IOException, com.coinbase.api2.exception.CoinbaseException {
        URL orderUrl;
        try {
            orderUrl = new URL(_baseApiUrl, "orders/" + idOrCustom);
        } catch (MalformedURLException ex) {
            throw new com.coinbase.api2.exception.CoinbaseException("Invalid order id/custom");
        }
        return get(orderUrl, com.coinbase.api2.entity.OrderResponse.class).getOrder();
    }

    @Override
    public com.coinbase.api2.entity.OrdersResponse getOrders(int page) throws IOException, com.coinbase.api2.exception.CoinbaseException {
        URL ordersUrl;
        try {
            ordersUrl = new URL(
                    _baseApiUrl,
                    "orders?page=" + page +
                            (_accountId != null ? "&account_id=" + _accountId : "")
            );
        } catch (MalformedURLException ex) {
            throw new com.coinbase.api2.exception.CoinbaseException("Invalid  account id");
        }
        return get(ordersUrl, com.coinbase.api2.entity.OrdersResponse.class);
    }

    @Override
    public com.coinbase.api2.entity.OrdersResponse getOrders() throws IOException, com.coinbase.api2.exception.CoinbaseException {
        return getOrders(1);
    }

    @Override
    public com.coinbase.api2.entity.AddressesResponse getAddresses(int page) throws IOException, com.coinbase.api2.exception.CoinbaseException {
        URL addressesUrl;
        try {
            addressesUrl = new URL(
                    _baseApiUrl,
                    "addresses?page=" + page +
                            (_accountId != null ? "&account_id=" + _accountId : "")
            );
        } catch (MalformedURLException ex) {
            throw new com.coinbase.api2.exception.CoinbaseException("Invalid account id");
        }
        return get(addressesUrl, com.coinbase.api2.entity.AddressesResponse.class);
    }

    @Override
    public com.coinbase.api2.entity.AddressesResponse getAddresses() throws IOException, com.coinbase.api2.exception.CoinbaseException {
        return getAddresses(1);
    }

    @Override
    public ContactsResponse getContacts(int page) throws IOException, com.coinbase.api2.exception.CoinbaseException {
        URL contactsUrl;
        try {
            contactsUrl = new URL(
                    _baseApiUrl,
                    "contacts?page=" + page
            );
        } catch (MalformedURLException ex) {
            throw new AssertionError(ex);
        }

        return get(contactsUrl, ContactsResponse.class);
    }

    @Override
    public ContactsResponse getContacts() throws IOException, com.coinbase.api2.exception.CoinbaseException {
        return getContacts(1);
    }

    @Override
    public com.coinbase.api2.entity.ContactsResponse getContacts(String query, int page) throws IOException, com.coinbase.api2.exception.CoinbaseException {
        URL contactsUrl;
        try {
            contactsUrl = new URL(
                    _baseApiUrl,
                    "contacts?page=" + page +
                            "&query=" + URLEncoder.encode(query, "UTF-8")
            );
        } catch (MalformedURLException ex) {
            throw new AssertionError(ex);
        }

        return get(contactsUrl, ContactsResponse.class);
    }

    @Override
    public ContactsResponse getContacts(String query) throws IOException, com.coinbase.api2.exception.CoinbaseException {
        return getContacts(query, 1);
    }

    @Override
    public Map<String, BigDecimal> getExchangeRates() throws IOException, com.coinbase.api2.exception.CoinbaseException {
        URL ratesUrl;
        try {
            ratesUrl = new URL(_baseApiUrl, "currencies/exchange_rates");
        } catch (MalformedURLException ex) {
            throw new AssertionError(ex);
        }
        return deserialize(doHttp(ratesUrl, "GET", null), new TypeReference<HashMap<String, BigDecimal>>() {});
    }

    @Override
    public List<CurrencyUnit> getSupportedCurrencies() throws IOException, com.coinbase.api2.exception.CoinbaseException {
        URL currenciesUrl;
        try {
            currenciesUrl = new URL(_baseApiUrl, "currencies");
        } catch (MalformedURLException ex) {
            throw new AssertionError(ex);
        }

        List<List<String>> rawResponse =
                deserialize(doHttp(currenciesUrl, "GET", null), new TypeReference<List<List<String>>>() {});

        List<CurrencyUnit> result = new ArrayList<CurrencyUnit>();
        for (List<String> currency : rawResponse) {
            try {
                result.add(CurrencyUnit.getInstance(currency.get(1)));
            } catch (IllegalCurrencyException ex) {}
        }

        return result;
    }

    @Override
    public List<com.coinbase.api2.entity.HistoricalPrice> getHistoricalPrices(int page) throws com.coinbase.api2.exception.CoinbaseException, IOException {
        URL historicalPricesUrl;
        try {
            historicalPricesUrl = new URL(
                    _baseApiUrl,
                    "prices/historical?page=" + page
            );
        } catch (MalformedURLException ex) {
            throw new AssertionError(ex);
        }

        String responseBody = doHttp(historicalPricesUrl, "GET", null);

        CSVReader reader = new CSVReader(new StringReader(responseBody));

        ArrayList<com.coinbase.api2.entity.HistoricalPrice> result = new ArrayList<com.coinbase.api2.entity.HistoricalPrice>();
        String[] nextLine;

        try {
            while ((nextLine = reader.readNext()) != null) {
                com.coinbase.api2.entity.HistoricalPrice hp = new com.coinbase.api2.entity.HistoricalPrice();
                hp.setTime(DateTime.parse(nextLine[0]));
                hp.setSpotPrice(Money.of(CurrencyUnit.USD, new BigDecimal(nextLine[1])));
                result.add(hp);
            }
        } catch (IOException e) {
            throw new com.coinbase.api2.exception.CoinbaseException("Error parsing csv response");
        } finally {
            try {
                reader.close();
            } catch (IOException e) {}
        }

        return result;
    }

    @Override
    public List<com.coinbase.api2.entity.HistoricalPrice> getHistoricalPrices() throws com.coinbase.api2.exception.CoinbaseException, IOException {
        return getHistoricalPrices(1);
    }

    @Override
    public com.coinbase.api2.entity.Button createButton(com.coinbase.api2.entity.Button button) throws com.coinbase.api2.exception.CoinbaseException, IOException {
        URL buttonsUrl;
        try {
            buttonsUrl = new URL(_baseApiUrl, "buttons");
        } catch (MalformedURLException ex) {
            throw new AssertionError(ex);
        }

        com.coinbase.api2.entity.Request request = newAccountSpecificRequest();
        request.setButton(button);

        return post(buttonsUrl, request, ButtonResponse.class).getButton();
    }

    @Override
    public com.coinbase.api2.entity.Order createOrder(com.coinbase.api2.entity.Button button) throws com.coinbase.api2.exception.CoinbaseException, IOException {
        URL ordersUrl;
        try {
            ordersUrl = new URL(_baseApiUrl, "orders");
        } catch (MalformedURLException ex) {
            throw new AssertionError(ex);
        }

        Request request = newAccountSpecificRequest();
        request.setButton(button);

        return post(ordersUrl, request, com.coinbase.api2.entity.OrderResponse.class).getOrder();
    }

    @Override
    public com.coinbase.api2.entity.Order createOrderForButton(String buttonCode) throws com.coinbase.api2.exception.CoinbaseException, IOException {
        URL createOrderForButtonUrl;
        try {
            createOrderForButtonUrl = new URL(_baseApiUrl, "buttons/" + buttonCode + "/create_order");
        } catch (MalformedURLException ex) {
            throw new com.coinbase.api2.exception.CoinbaseException("Invalid button code");
        }

        return post(createOrderForButtonUrl, new Request(), com.coinbase.api2.entity.OrderResponse.class).getOrder();
    }

    @Override
    public com.coinbase.api2.entity.PaymentMethodsResponse getPaymentMethods() throws IOException, com.coinbase.api2.exception.CoinbaseException {
        URL paymentMethodsUrl;
        try {
            paymentMethodsUrl = new URL(_baseApiUrl, "payment_methods");
        } catch (MalformedURLException ex) {
            throw new AssertionError(ex);
        }

        return get(paymentMethodsUrl, com.coinbase.api2.entity.PaymentMethodsResponse.class);
    }

    @Override
    public com.coinbase.api2.entity.RecurringPaymentsResponse getSubscribers(int page) throws IOException, com.coinbase.api2.exception.CoinbaseException {
        URL subscribersUrl;
        try {
            subscribersUrl = new URL(
                    _baseApiUrl,
                    "subscribers?page=" + page
            );
        } catch (MalformedURLException ex) {
            throw new AssertionError(ex);
        }

        return get(subscribersUrl, com.coinbase.api2.entity.RecurringPaymentsResponse.class);
    }

    @Override
    public com.coinbase.api2.entity.RecurringPaymentsResponse getSubscribers() throws IOException, com.coinbase.api2.exception.CoinbaseException {
        return getSubscribers(1);
    }

    @Override
    public com.coinbase.api2.entity.RecurringPaymentsResponse getRecurringPayments(int page) throws IOException, com.coinbase.api2.exception.CoinbaseException {
        URL recurringPaymentsUrl;
        try {
            recurringPaymentsUrl = new URL(
                    _baseApiUrl,
                    "recurring_payments?page=" + page
            );
        } catch (MalformedURLException ex) {
            throw new AssertionError(ex);
        }
        return get(recurringPaymentsUrl, com.coinbase.api2.entity.RecurringPaymentsResponse.class);
    }

    @Override
    public com.coinbase.api2.entity.RecurringPaymentsResponse getRecurringPayments() throws IOException, com.coinbase.api2.exception.CoinbaseException {
        return getRecurringPayments(1);
    }

    @Override
    public RecurringPayment getRecurringPayment(String id) throws com.coinbase.api2.exception.CoinbaseException, IOException {
        URL recurringPaymentUrl;
        try {
            recurringPaymentUrl = new URL(_baseApiUrl, "recurring_payments/" + id);
        } catch (MalformedURLException ex) {
            throw new com.coinbase.api2.exception.CoinbaseException("Invalid payment id");
        }

        return get(recurringPaymentUrl, RecurringPaymentResponse.class).getRecurringPayment();
    }

    @Override
    public RecurringPayment getSubscriber(String id) throws com.coinbase.api2.exception.CoinbaseException, IOException {
        URL subscriberUrl;
        try {
            subscriberUrl = new URL(_baseApiUrl, "subscribers/" + id);
        } catch (MalformedURLException ex) {
            throw new com.coinbase.api2.exception.CoinbaseException("Invalid subscriber id");
        }

        return get(subscriberUrl, RecurringPaymentResponse.class).getRecurringPayment();
    }

    @Override
    public AddressResponse generateReceiveAddress(com.coinbase.api2.entity.Address addressParams) throws com.coinbase.api2.exception.CoinbaseException, IOException {
        URL generateAddressUrl;
        try {
            generateAddressUrl = new URL(_baseApiUrl, "account/generate_receive_address");
        } catch (MalformedURLException ex) {
            throw new AssertionError(ex);
        }

        com.coinbase.api2.entity.Request request = newAccountSpecificRequest();
        request.setAddress(addressParams);

        return post(generateAddressUrl, request, AddressResponse.class);
    }

    @Override
    public AddressResponse generateReceiveAddress() throws com.coinbase.api2.exception.CoinbaseException, IOException {
        return generateReceiveAddress(null);
    }

    @Override
    public User createUser(com.coinbase.api2.entity.User userParams) throws com.coinbase.api2.exception.CoinbaseException, IOException {
        URL usersUrl;
        try {
            usersUrl = new URL(_baseApiUrl, "users");
        } catch (MalformedURLException ex) {
            throw new AssertionError(ex);
        }

        com.coinbase.api2.entity.Request request = new Request();
        request.setUser(userParams);

        return post(usersUrl, request, com.coinbase.api2.entity.UserResponse.class).getUser();
    }

    @Override
    public User createUser(com.coinbase.api2.entity.User userParams, String clientId, String scope) throws com.coinbase.api2.exception.CoinbaseException, IOException {
        URL usersUrl;
        try {
            usersUrl = new URL(_baseApiUrl, "users");
        } catch (MalformedURLException ex) {
            throw new AssertionError(ex);
        }

        com.coinbase.api2.entity.Request request = new Request();
        request.setUser(userParams);
        request.setScopes(scope);
        request.setClientId(clientId);

        return post(usersUrl, request, com.coinbase.api2.entity.UserResponse.class).getUser();
    }

    @Override
    public com.coinbase.api2.entity.UserResponse createUserWithOAuth(com.coinbase.api2.entity.User userParams, String clientId, String scope) throws com.coinbase.api2.exception.CoinbaseException, IOException {
        URL usersUrl;
        try {
            usersUrl = new URL(_baseApiUrl, "users");
        } catch (MalformedURLException ex) {
            throw new AssertionError(ex);
        }

        Request request = new Request();
        request.setUser(userParams);
        request.setScopes(scope);
        request.setClientId(clientId);

        return post(usersUrl, request, com.coinbase.api2.entity.UserResponse.class);
    }

    @Override
    public User updateUser(String userId, User userParams) throws com.coinbase.api2.exception.CoinbaseException, IOException {
        URL userUrl;
        try {
            userUrl = new URL(_baseApiUrl, "users/" + userId);
        } catch (MalformedURLException ex) {
            throw new com.coinbase.api2.exception.CoinbaseException("Invalid user id");
        }

        com.coinbase.api2.entity.Request request = new Request();
        request.setUser(userParams);

        return put(userUrl, request, com.coinbase.api2.entity.UserResponse.class).getUser();
    }

    @Override
    public com.coinbase.api2.entity.Token createToken() throws com.coinbase.api2.exception.CoinbaseException, IOException {
        URL tokensUrl;
        try {
            tokensUrl = new URL(_baseApiUrl, "tokens");
        } catch (MalformedURLException ex) {
            throw new AssertionError(ex);
        }

        return post(tokensUrl, new com.coinbase.api2.entity.Request(), TokenResponse.class).getToken();
    }

    @Override
    public void redeemToken(String tokenId) throws com.coinbase.api2.exception.CoinbaseException, IOException {
        URL redeemTokenUrl;
        try {
            redeemTokenUrl = new URL(_baseApiUrl, "tokens/redeem");
        } catch (MalformedURLException ex) {
            throw new AssertionError(ex);
        }

        Request request = new com.coinbase.api2.entity.Request();
        request.setTokenId(tokenId);

        post(redeemTokenUrl, request, com.coinbase.api2.entity.Response.class);
    }

    @Override
    public Application createApplication(com.coinbase.api2.entity.Application applicationParams) throws com.coinbase.api2.exception.CoinbaseException, IOException {
        URL applicationsUrl;
        try {
            applicationsUrl = new URL(_baseApiUrl, "oauth/applications");
        } catch (MalformedURLException ex) {
            throw new AssertionError(ex);
        }

        Request request = new com.coinbase.api2.entity.Request();
        request.setApplication(applicationParams);

        return post(applicationsUrl, request, com.coinbase.api2.entity.ApplicationResponse.class).getApplication();
    }

    @Override
    public com.coinbase.api2.entity.ApplicationsResponse getApplications() throws IOException, com.coinbase.api2.exception.CoinbaseException {
        URL applicationsUrl;
        try {
            applicationsUrl = new URL(_baseApiUrl, "oauth/applications");
        } catch (MalformedURLException ex) {
            throw new AssertionError(ex);
        }
        return get(applicationsUrl, ApplicationsResponse.class);
    }

    @Override
    public Application getApplication(String id) throws IOException, CoinbaseException {
        URL applicationUrl;
        try {
            applicationUrl = new URL(_baseApiUrl, "oauth/applications/" + id);
        } catch (MalformedURLException ex) {
            throw new com.coinbase.api2.exception.CoinbaseException("Invalid application id");
        }
        return get(applicationUrl, ApplicationResponse.class).getApplication();
    }

    @Override
    public com.coinbase.api2.entity.Report createReport(com.coinbase.api2.entity.Report reportParams) throws com.coinbase.api2.exception.CoinbaseException, IOException {
        URL reportsUrl;
        try {
            reportsUrl = new URL(_baseApiUrl, "reports");
        } catch (MalformedURLException ex) {
            throw new AssertionError(ex);
        }

        com.coinbase.api2.entity.Request request = newAccountSpecificRequest();
        request.setReport(reportParams);

        return post(reportsUrl, request, ReportResponse.class).getReport();
    }

    @Override
    public com.coinbase.api2.entity.Report getReport(String reportId) throws IOException, com.coinbase.api2.exception.CoinbaseException {
        URL reportUrl;
        try {
            reportUrl = new URL(
                    _baseApiUrl,
                    "reports/" + reportId +
                            (_accountId != null ? "?account_id=" + _accountId : "")
            );
        } catch (MalformedURLException ex) {
            throw new com.coinbase.api2.exception.CoinbaseException("Invalid report id");
        }
        return get(reportUrl, ReportResponse.class).getReport();
    }

    @Override
    public ReportsResponse getReports(int page) throws IOException, com.coinbase.api2.exception.CoinbaseException {
        URL reportsUrl;
        try {
            reportsUrl = new URL(
                    _baseApiUrl,
                    "reports?page=" + page +
                            (_accountId != null ? "&account_id=" + _accountId : "")
            );
        } catch (MalformedURLException ex) {
            throw new com.coinbase.api2.exception.CoinbaseException("Invalid account id");
        }
        return get(reportsUrl, ReportsResponse.class);
    }

    @Override
    public com.coinbase.api2.entity.ReportsResponse getReports() throws IOException, com.coinbase.api2.exception.CoinbaseException {
        return getReports(1);
    }

    @Override
    public com.coinbase.api2.entity.AccountChangesResponse getAccountChanges(int page) throws IOException, com.coinbase.api2.exception.CoinbaseException {
        URL accountChangesUrl;
        try {
            accountChangesUrl = new URL(
                    _baseApiUrl,
                    "account_changes?page=" + page +
                            (_accountId != null ? "&account_id=" + _accountId : "")
            );
        } catch (MalformedURLException ex) {
            throw new com.coinbase.api2.exception.CoinbaseException("Invalid account id");
        }
        return get(accountChangesUrl, AccountChangesResponse.class);
    }

    @Override
    public com.coinbase.api2.entity.AccountChangesResponse getAccountChanges() throws IOException, com.coinbase.api2.exception.CoinbaseException {
        return getAccountChanges(1);
    }

    @Override
    public String getAuthCode(com.coinbase.api2.entity.OAuthCodeRequest request)
            throws com.coinbase.api2.exception.CoinbaseException, IOException {

        URL credentialsAuthorizationUrl;
        try {
            credentialsAuthorizationUrl = new URL(_baseOAuthUrl, "authorize/with_credentials");
        } catch (MalformedURLException ex) {
            throw new AssertionError(ex);
        }

        return post(credentialsAuthorizationUrl, request, OAuthCodeResponse.class).getCode();
    }

    @Override
    public com.coinbase.api2.entity.OAuthTokensResponse getTokens(String clientId, String clientSecret, String authCode, String redirectUri)
            throws com.coinbase.api2.exception.UnauthorizedDeviceException, com.coinbase.api2.exception.CoinbaseException, IOException {

        URL tokenUrl;
        try {
            tokenUrl = new URL(_baseOAuthUrl, "token");
        } catch (MalformedURLException ex) {
            throw new AssertionError(ex);
        }

        OAuthTokensRequest request = new com.coinbase.api2.entity.OAuthTokensRequest();
        request.setClientId(clientId);
        request.setClientSecret(clientSecret);
        request.setGrantType(com.coinbase.api2.entity.OAuthTokensRequest.GrantType.AUTHORIZATION_CODE);
        request.setCode(authCode);
        request.setRedirectUri(redirectUri != null? redirectUri : "2_legged");

        return post(tokenUrl, request, com.coinbase.api2.entity.OAuthTokensResponse.class);
    }

    @Override
    public void revokeToken() throws com.coinbase.api2.exception.CoinbaseException, IOException {

        if (_accessToken == null) {
            throw new com.coinbase.api2.exception.CoinbaseException(
                "This client must have been initialized with an access token in order to call revokeToken()"
            );
        }

        URL revokeTokenUrl;
        try {
            revokeTokenUrl = new URL(_baseOAuthUrl, "revoke");
        } catch (MalformedURLException ex) {
            throw new AssertionError(ex);
        }

        com.coinbase.api2.entity.RevokeTokenRequest request = new com.coinbase.api2.entity.RevokeTokenRequest();
        request.setToken(_accessToken);

        post(revokeTokenUrl, request, com.coinbase.api2.entity.Response.class);
        _accessToken = null;
    }

    @Override
    public com.coinbase.api2.entity.OAuthTokensResponse refreshTokens(String clientId, String clientSecret, String refreshToken)
            throws com.coinbase.api2.exception.CoinbaseException, IOException {

        URL tokenUrl;
        try {
            tokenUrl = new URL(_baseOAuthUrl, "token");
        } catch (MalformedURLException ex) {
            throw new AssertionError(ex);
        }

        com.coinbase.api2.entity.OAuthTokensRequest request = new com.coinbase.api2.entity.OAuthTokensRequest();
        request.setClientId(clientId);
        request.setClientSecret(clientSecret);
        request.setGrantType(com.coinbase.api2.entity.OAuthTokensRequest.GrantType.REFRESH_TOKEN);
        request.setRefreshToken(refreshToken);

        return post(tokenUrl, request, OAuthTokensResponse.class);
    }

    @Override
    public void sendSMS(String clientId, String clientSecret, String email, String password) throws com.coinbase.api2.exception.CoinbaseException, IOException {

        URL smsUrl;
        try {
            smsUrl = new URL(_baseOAuthUrl, "authorize/with_credentials/sms_token");
        } catch (MalformedURLException ex) {
            throw new AssertionError(ex);
        }

        com.coinbase.api2.entity.OAuthCodeRequest request = new com.coinbase.api2.entity.OAuthCodeRequest();
        request.setClientId(clientId);
        request.setClientSecret(clientSecret);
        request.setUsername(email);
        request.setPassword(password);

        post(smsUrl, request, com.coinbase.api2.entity.Response.class);
    }

    @Override
    public URI getAuthorizationUri(com.coinbase.api2.entity.OAuthCodeRequest params) throws com.coinbase.api2.exception.CoinbaseException {
        URL authorizeURL;
        URIBuilder uriBuilder;

        try {
            authorizeURL = new URL(_baseOAuthUrl, "authorize");
            uriBuilder = new URIBuilder(authorizeURL.toURI());
        } catch (URISyntaxException ex) {
            throw new AssertionError(ex);
        } catch (MalformedURLException ex) {
            throw new AssertionError(ex);
        }

        uriBuilder.addParameter("response_type", "code");

        if (params.getClientId() != null) {
            uriBuilder.addParameter("client_id", params.getClientId());
        } else {
            throw new com.coinbase.api2.exception.CoinbaseException("client_id is required");
        }

        if (params.getRedirectUri() != null) {
            uriBuilder.addParameter("redirect_uri", params.getRedirectUri());
        } else {
            throw new com.coinbase.api2.exception.CoinbaseException("redirect_uri is required");
        }

        if (params.getScope() != null) {
            uriBuilder.addParameter("scope", params.getScope());
        } else {
            throw new com.coinbase.api2.exception.CoinbaseException("scope is required");
        }

        if (params.getMeta() != null) {
            com.coinbase.api2.entity.OAuthCodeRequest.Meta meta = params.getMeta();

            if (meta.getName() != null) {
                uriBuilder.addParameter("meta[name]", meta.getName());
            }
            if (meta.getSendLimitAmount() != null) {
                Money sendLimit = meta.getSendLimitAmount();
                uriBuilder.addParameter("meta[send_limit_amount]", sendLimit.getAmount().toPlainString());
                uriBuilder.addParameter("meta[send_limit_currency]", sendLimit.getCurrencyUnit().getCurrencyCode());
                if (meta.getSendLimitPeriod() != null) {
                    uriBuilder.addParameter("meta[send_limit_period]", meta.getSendLimitPeriod().toString());
                }
            }
        }

        try {
            return uriBuilder.build();
        } catch (URISyntaxException e) {
            throw new AssertionError(e);
        }
    }

    @Override
    public boolean verifyCallback(String body, String signature) {
        return _callbackVerifier.verifyCallback(body, signature);
    }

    private void doHmacAuthentication (URL url, String body, HttpsURLConnection conn) throws IOException {
        String nonce = String.valueOf(System.currentTimeMillis());

        String message = nonce + url.toString() + (body != null ? body : "");

        Mac mac = null;
        try {
            mac = Mac.getInstance("HmacSHA256");
            mac.init(new SecretKeySpec(_apiSecret.getBytes(), "HmacSHA256"));
        } catch (Throwable t) {
            throw new IOException(t);
        }

        String signature = new String(Hex.encodeHex(mac.doFinal(message.getBytes())));

        conn.setRequestProperty("ACCESS_KEY", _apiKey);
        conn.setRequestProperty("ACCESS_SIGNATURE", signature);
        conn.setRequestProperty("ACCESS_NONCE", nonce);
    }

    private void doAccessTokenAuthentication(HttpsURLConnection conn) {
        conn.setRequestProperty("Authorization", "Bearer " + _accessToken);
		conn.setRequestProperty("CB-VERSION", "2016-05-11");
    }

    private String doHttp(URL url, String method, Object requestBody) throws IOException, com.coinbase.api2.exception.CoinbaseException {
        URLConnection urlConnection = url.openConnection();
        if (!(urlConnection instanceof HttpsURLConnection)) {
            throw new RuntimeException(
                    "Custom Base URL must return javax.net.ssl.HttpsURLConnection on openConnection.");
        }
        HttpsURLConnection conn = (HttpsURLConnection) urlConnection;
        conn.setSSLSocketFactory(_socketFactory);
        conn.setRequestMethod(method);

        String body = null;
        if (requestBody != null) {
            body = objectMapper.writeValueAsString(requestBody);
            conn.setRequestProperty("Content-Type", "application/json");
        }

        if (_apiKey != null && _apiSecret != null) {
            doHmacAuthentication(url, body, conn);
        } else if (_accessToken != null) {
            doAccessTokenAuthentication(conn);
        }

        if (body != null) {
            conn.setDoOutput(true);
            OutputStream outputStream = conn.getOutputStream();
            try {
                outputStream.write(body.getBytes(Charset.forName("UTF-8")));
            } finally {
                outputStream.close();
            }
        }

        InputStream is = null;
        InputStream es = null;
        try {
            is = conn.getInputStream();
            return IOUtils.toString(is, "UTF-8");
        } catch (IOException e) {
            es = conn.getErrorStream();
            String errorBody = null;
            if (es != null) {
                errorBody = IOUtils.toString(es, "UTF-8");
                if (errorBody != null && conn.getContentType().toLowerCase().contains("json")) {
                    Response coinbaseResponse;
                    try {
                        coinbaseResponse = deserialize(errorBody, Response.class);
                    } catch (Exception ex) {
                        throw new CoinbaseException(errorBody);
                    }
                    handleErrors(coinbaseResponse);
                }
            }
            if (HttpsURLConnection.HTTP_UNAUTHORIZED == conn.getResponseCode()) {
                throw new UnauthorizedException(errorBody);
            }
            throw e;
        } finally {
            if (is != null) {
                is.close();
            }

            if (es != null) {
                es.close();
            }
        }
    }

    private static <T> T deserialize(String json, Class<T> clazz) throws IOException {
        return objectMapper.readValue(json, clazz);
    }

    private static <T> T deserialize(String json, TypeReference<T> typeReference) throws IOException {
        return objectMapper.readValue(json, typeReference);
    }

    private <T extends Response> T get(URL url, Class<T> responseClass) throws IOException, CoinbaseException {
        return handleErrors(deserialize(doHttp(url, "GET", null), responseClass));
    }

    private <T extends Response> T post(URL url, Object entity, Class<T> responseClass) throws CoinbaseException, IOException {
        return handleErrors(deserialize(doHttp(url, "POST", entity), responseClass));
    }

    private <T extends Response> T put(URL url, Object entity, Class<T> responseClass) throws com.coinbase.api2.exception.CoinbaseException, IOException {
        return handleErrors(deserialize(doHttp(url, "PUT", entity), responseClass));
    }

    private <T extends com.coinbase.api2.entity.Response> T delete(URL url, Class<T> responseClass) throws com.coinbase.api2.exception.CoinbaseException, IOException {
        return handleErrors(deserialize(doHttp(url, "DELETE", null), responseClass));
    }

    private static <T extends Response> T handleErrors(T response) throws CoinbaseException {
        String errors = response.getErrors();
        if (errors != null) {
            if (errors.contains("device_confirmation_required")) {
                throw new com.coinbase.api2.exception.UnauthorizedDeviceException();
            } else if (errors.contains("2fa_required")) {
                throw new com.coinbase.api2.exception.TwoFactorRequiredException();
            } else if (errors.contains("2fa_incorrect")) {
                throw new TwoFactorIncorrectException();
            } else if (errors.contains("incorrect_credentials")) {
                throw new CredentialsIncorrectException();
            }
            throw new CoinbaseException(response.getErrors());
        }

        if (response.isSuccess() != null && !response.isSuccess()) {
            throw new CoinbaseException("Unknown error");
        }

        return response;
    }

    private com.coinbase.api2.entity.Request newAccountSpecificRequest() {
        com.coinbase.api2.entity.Request request = new com.coinbase.api2.entity.Request();
        if (_accountId != null) {
            request.setAccountId(_accountId);
        }
        return request;
    }
}
