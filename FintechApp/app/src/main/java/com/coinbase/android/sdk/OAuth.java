package com.coinbase.android.sdk;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.Random;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;

import com.coinbase.api2.Coinbase;
import com.coinbase.api2.CoinbaseBuilder;
import com.coinbase.api2.entity.OAuthCodeRequest;
import com.coinbase.api2.entity.OAuthTokensResponse;
import com.coinbase.api2.exception.CoinbaseException;
import com.coinbase.api2.exception.UnauthorizedException;

public class OAuth {

    private static final String KEY_COINBASE_PREFERENCES = "com.coinbase.android.sdk";
    private static final String KEY_LOGIN_CSRF_TOKEN = "com.coinbase.android.sdk.login_csrf_token";
    public static final String URL_BASE_API_SANDBOX="https://api.sandbox.coinbase.com/v1/";
    public static final String URL_BASE_OAUTH_SANDBOX="https://sandbox.coinbase.com/oauth/";

    public static void beginAuthorization(Context context, String clientId,
            String scope, String redirectUri, OAuthCodeRequest.Meta meta)
            throws CoinbaseException {

        Coinbase coinbase = null;
        try {
            coinbase = new CoinbaseBuilder().withBaseApiURL(new URL(URL_BASE_API_SANDBOX)).withBaseOAuthURL(new URL(URL_BASE_OAUTH_SANDBOX)).build();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        OAuthCodeRequest request = new OAuthCodeRequest();
        request.setClientId(clientId);
        request.setScope(scope);
        request.setRedirectUri(redirectUri);
        request.setMeta(meta);

        URI authorizationUri = coinbase.getAuthorizationUri(request);

        Intent i = new Intent(Intent.ACTION_VIEW);
        Uri androidUri = Uri.parse(authorizationUri.toString());
        androidUri = androidUri.buildUpon().appendQueryParameter("state", getLoginCSRFToken(context)).build();
        i.setData(androidUri);
        context.startActivity(i);
    }

    public static OAuthTokensResponse completeAuthorization(Context context, String clientId,
            String clientSecret, Uri redirectUri) throws UnauthorizedException, IOException {

        String csrfToken = redirectUri.getQueryParameter("state");
        String authCode = redirectUri.getQueryParameter("code");

        if (csrfToken == null || !csrfToken.equals(getLoginCSRFToken(context))) {
            throw new UnauthorizedException("CSRF Detected!");
        } else if (authCode == null) {
            String errorDescription = redirectUri.getQueryParameter("error_description");
            throw new UnauthorizedException(errorDescription);
        } else {
            try {
                Coinbase coinbase = new CoinbaseBuilder().withBaseApiURL(new URL(URL_BASE_API_SANDBOX)).withBaseOAuthURL(new URL(URL_BASE_OAUTH_SANDBOX)).build();
                Uri redirectUriWithoutQuery = redirectUri.buildUpon().clearQuery().build();
                return coinbase.getTokens(clientId, clientSecret, authCode, redirectUriWithoutQuery.toString());
            } catch (CoinbaseException ex) {
                throw new UnauthorizedException(ex.getMessage());
            }
        }
    }

    public static String getLoginCSRFToken(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(KEY_COINBASE_PREFERENCES, Context.MODE_PRIVATE);

        int result = prefs.getInt(KEY_LOGIN_CSRF_TOKEN, 0);
        if (result == 0) {
            result = (new Random()).nextInt();
            SharedPreferences.Editor e = prefs.edit();
            e.putInt(KEY_LOGIN_CSRF_TOKEN, result);
            e.commit();
        }

        return Integer.toString(result);
    }
}
